<?php


$source = 'http://www.smh.com.au/';

$smh_xpath = domXpath($source);

//get menus
$menus = $smh_xpath->query('// li[@class="nav__item"]');
$menusHref = $smh_xpath->query('// li[@class="nav__item"]/a/@href');

//associates href per menu
    $menus = domToArray($menus);
    $menusHref = domToArray($menusHref);

    $categories = [];
    $x = 0;
    foreach($menus as $menu){
        $categories[$menu] = $menusHref[$x];
        $x++;
    }
//END /associates href per menu
asort($categories);

//DISPLAY
foreach($categories as $category=>$href){
   echo '<h1>' . $category . '</h1>' ;
    //check if valid href
    $pos = substr($href, 0, 1);
    if($pos == '/')
        $items =  getItems($source . $href);
    else
        $items = 'Page invalid';
    echo $items;
}
//END of DIPLAY

function getItems($hrefVal){  
    //getArticles
    $smh_xpath = domXpath($hrefVal);
    $articles = $smh_xpath->query('// article[@class="story"]/div/h3[@class="story__headline"]');

    //get images
    $images = $smh_xpath->query('// article[@class="story"]/figure/a/img/@src');

    //Associate Image To Article
    $imagesArray = domToArray($images);
    $articlesArray = domToArray($articles);
    $ArticleImgs =[];
    $x = 0;
    foreach($articlesArray as $article){
        $row = [ 
            'article' => $article, 
            'image' => (isset($imagesArray[$x]))? $imagesArray[$x] : ''
        ];
        $ArticleImgs[$x] = $row;
        $x++;
    }
    // END Associate Image To Article

    return  arrayToLi(sortArray($ArticleImgs), 'Articles');
}    

//DOM object to array
function domToArray($object)
{
    $list = [];
    foreach($object as $row)
        array_push($list,$row->nodeValue);
    return $list;
}

//return sort array
function sortArray($array){
    sort($array);
    $sortedList = [];
    foreach($array as $x => $value) {
        array_push($sortedList,$value);
    }
    return $sortedList;
}

//array to list
function arrayToLi($list, $sub){
    $li = '';
    $li .=  '<h2>'.$sub.'</h2> <ul>';
    foreach($list as $item){
        $li .= '<li> <strong>' . $item['article'] . '</strong>'; 
        $li .= '//img_src='. $item['image'];
        $li .= '</li>';
    }
    $li .= '</ul>';
    return $li;
}

//get DOMXpath object
function domXpath($source){
    $html = file_get_contents($source); 
    $smh = new DOMDocument();
    libxml_use_internal_errors(TRUE);
    $smh->loadHTML($html);
    $smh_xpath = new DOMXPath($smh);
    
    return $smh_xpath;
}

?>
